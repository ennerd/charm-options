<?php
namespace Charm\Options;

interface ExceptionInterface {

    const UNKNOWN_OPTION = 1;
    const ILLEGAL_VALUE = 2;
    const ILLEGAL_OPERATION = 3;

}
