<?php
namespace Charm\Options;

use Exception, ReflectionProperty;
use Charm\AbstractOptions;

class IllegalValueException extends Exception implements ExceptionInterface {
    public function __construct(AbstractOptions $options, string $key, $value, array $enums=null) {

        if ($enums !== null) {
            parent::__construct("Option '".get_class($options)."::\$$key' must be one of ".implode(" or ", $enums));
            return;
        }

        $receivedType = (
            is_scalar($value) ? ['integer' => 'int', 'double' => 'float', 'boolean' => 'bool'][gettype($value)] ?? gettype($value) : (
            is_object($value) ? get_class($value) : "null")
        );

        $expectedType = new ReflectionProperty($options, $key);
        if (!$expectedType->hasType()) {
            parent::__construct("Option '".get_class($options)."::\$$key' is untyped so it must be set with a scalar value", ExceptionInterface::ILLEGAL_VALUE);
        } else {
            $expectedType = (string) $expectedType->getType();
            parent::__construct("Option '".get_class($options)."::\$$key' expects a value of type '$expectedType', tried setting a value of type '$receivedType'.", ExceptionInterface::ILLEGAL_VALUE);
        }
    }
}
