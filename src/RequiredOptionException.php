<?php
namespace Charm\Options;

use Exception, ReflectionProperty;
use Charm\AbstractOptions;

class RequiredOptionException extends Exception implements ExceptionInterface {
    public function __construct(AbstractOptions $options, string $key) {
        parent::__construct("Option '".get_class($options)."::\$$key' is required");
    }
}
