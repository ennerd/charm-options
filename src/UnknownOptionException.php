<?php
namespace Charm\Options;

use ErrorException;
use Charm\AbstractOptions;

class UnknownOptionException extends \ErrorException implements ExceptionInterface {
    public function __construct(AbstractOptions $options, string $key) {
        parent::__construct("Unknown option '".get_class($options)."::\$$key'", ExceptionInterface::UNKNOWN_OPTION);
    }
}
