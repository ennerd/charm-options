<?php
namespace Charm\Options;

use LogicException;

class IllegalOperationException extends LogicException implements ExceptionInterface {
    public function __construct(string $description) {
        parent::__construct($description, ExceptionInterface::ILLEGAL_OPERATION);
    }
}
