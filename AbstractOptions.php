<?php

declare(strict_types=1);

namespace Charm;

use Charm\Options\IllegalOperationException;
use Charm\Options\UnknownOptionException;

/**
 * Options class
 *
 * Inherits from {@see Charm\AbstractOptions}:
 *
 * A base class for validating and specifying options that can be provided
 * as an array or as an instance of the extending class. This provides
 * type safe options with validation. Declare your options class and
 * use it like this:
 *
 * ```php
 * class MyClass {
 *     public MyOptions $options; // MyOptions extends Charm\AbstractOptions
 *
 *     public function __construct(MyOptions|array|null $options=null) {
 *         $this->options = MyOptions::create($options);
 *     }
 * }
 * ```
 */
abstract class AbstractOptions implements \ArrayAccess, \IteratorAggregate
{
    /**
     * Create an options instance from a function argument.
     *
     * @param array|static|null $options
     *
     * @return static
     */
    public static function create(array|self|null $options): self
    {
        if (null === $options) {
            return new static();
        }
        if (\is_array($options)) {
            return new static($options);
        }
        if ($options instanceof static) {
            $ro                = new \ReflectionObject($options);
            $configuredOptions = [];
            foreach ($ro->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE) as $rp) {
                if ('_constructed_' === $rp->getName()) {
                    continue;
                }
                $rp->setAccessible(true);
                $configuredOptions[$rp->getName()] = $rp->getValue($options);
                $rp->setAccessible(false);
            }

            return new ($options::class)($configuredOptions);
        }
        throw new IllegalOperationException('Options must be provided as an array or as an instance of ' . static::class . '. ' . \get_debug_type($options) . ' received');
    }

    /**
     * See the implementing class properties for documentation for all available options.
     */
    public function __construct(array $options=[])
    {
        $this->_options_ = $options;
        foreach ($options as $key => $val) {
            $this[$key] = $val;
        }

        /*
         * Options without a default value are always required. Untyped properties are automatically given
         * the initial value `null` by PHP and are not required.
         */
        foreach ((new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PUBLIC) as $rp) {
            $rp->setAccessible(true);
            $inited = $rp->isInitialized($this);
            $rp->setAccessible(false);
            if (!$inited) {
                throw new Options\RequiredOptionException($this, $rp->getName());
            }
        }

        $this->finalize();

        $this->_constructed_ = true;
    }

    /**
     * Provides iteration over the configured options.
     *
     * @return \Traversable<mixed, mixed>|mixed[]
     */
    public function getIterator(): \Traversable
    {
        /*
         * Options without a default value are always required. Untyped properties are automatically given
         * the initial value `null` by PHP and are not required.
         */
        return new \ArrayIterator($this->__debugInfo());
    }

    /**
     * Create a new options instance with new options imported from
     * another options instance or an array.
     *
     * @throws UnknownOptionException
     * @throws IllegalOperationException
     */
    public function overrideFrom(array|self|null $opts=null): static
    {
        if (null === $opts) {
            return clone $this;
        }

        if ($opts instanceof self && null !== $opts->_options_) {
            $opts = $opts->_options_;
        }

        $newOpts = [];
        foreach ($this as $key => $value) {
            if ($opts instanceof self) {
                $rp = new \ReflectionProperty(static::class, $key);
                if ($rp->hasDefaultValue() && $opts->$key !== $rp->getDefaultValue()) {
                    $newOpts[$key] = $opts->$key;
                } elseif ($rp->hasDefaultValue() && $rp->getDefaultValue() !== $value) {
                    $newOpts[$key] = $value;
                }
            } elseif (\array_key_exists($key, $opts)) {
                $newOpts[$key] = $opts[$key];
            } else {
                $newOpts[$key] = $value;
            }
        }

        return static::create($newOpts);
    }

    /**
     * Override this method if you wish to set options automatically
     * unless they were configured.
     */
    protected function finalize()
    {
    }

    final public function __set($key, $value)
    {
        $this->offsetSet($key, $value);
    }

    final public function __get(mixed $key)
    {
        return $this->offsetGet($key);
    }

    final public function __unset(mixed $key)
    {
        $this->offsetUnset($key);
    }

    final public function __exists(mixed $key)
    {
        return $this->offsetExists($key);
    }

    final public function offsetSet(mixed $key, mixed $value): void
    {
        if ($this->_constructed_ || '_constructed_' === $key) {
            throw new IllegalOperationException("Can't modify option `$key`; the value must be provided during initialization");
        }

        if (!\property_exists($this, $key)) {
            throw new UnknownOptionException($this, $key);
        }

        // check for class constants
        $keyUpper  = \strtoupper($key);
        $keyLength = \strlen($key);
        $enums     = null;
        $enumValid = false;
        foreach ((new \ReflectionObject($this))->getConstants() as $constantName => $constantValue) {
            if (\substr($constantName, 0, $keyLength + 1) === $keyUpper . '_') {
                $enums[] = \get_class($this) . '::' . $constantName;
                if ($constantValue === $value) {
                    $enumValid = true;
                    break;
                }
            }
        }

        if (null !== $enums && !$enumValid) {
            throw new Options\IllegalValueException($this, $key, $value, $enums);
        }

        $rp = new \ReflectionProperty($this, $key);
        if (\method_exists($rp, 'hasType') && $rp->hasType()) {
            if (\is_array($value) && \class_exists((string) $rp->getType())) {
                $className = (string) $rp->getType();
                $value     = new $className($value);
            }

            // type check (since PHP will silently cast values)
            $typeChecker = function ($type, $value) use (&$typeChecker) {
                if (null === $value) {
                    return $type->allowsNull();
                }
                if (\method_exists($type, 'getTypes')) {
                    foreach ($type->getTypes() as $subType) {
                        if ($typeChecker($subType, $value)) {
                            return true;
                        }
                    }

                    return false;
                }
                if ($type->isBuiltin()) {
                    $receivedType = ['integer' => 'int', 'double' => 'float', 'boolean' => 'bool'][\gettype($value)] ?? \gettype($value);

                    if ($receivedType === $type->getName()) {
                        return true;
                    }

                    if ('int' === $receivedType && 'float' === $type->getName()) {
                        return true;
                    }

                    return false;
                }
                $typeName = $type->getName();

                return $value instanceof $typeName;
            };

            $type = $rp->getType();
            if (!$typeChecker($type, $value)) {
                throw new Options\IllegalValueException($this, $key, $value);
            }
        }

        $rp->setAccessible(true);
        $rp->setValue($this, $value);
        $rp->setAccessible(false);
    }

    final public function offsetGet(mixed $key): mixed
    {
        if (!\property_exists($this, $key)) {
            throw new UnknownOptionException($this, $key);
        }

        return $this->$key ?? null;
    }

    final public function offsetExists(mixed $key): bool
    {
        return \property_exists($this, $key);
    }

    final public function offsetUnset(mixed $key): void
    {
        if (!\property_exists($this, $key)) {
            throw new UnknownOptionException($this, $key);
        }
        throw new IllegalOperationException("Can't unset option values");
    }

    final public function __serialize()
    {
        return $this->__debugInfo();
    }

    final public function __unserialize(array $data)
    {
        $this->_constructed_ = false;
        foreach ($data as $key => $val) {
            $this->offsetSet($key, $val);
        }
        $this->_constructed_ = true;
    }

    final public function asArray(): array
    {
        return $this->__debugInfo();
    }

    final public function asObject()
    {
        return (object) $this->asArray();
    }

    final public function __debugInfo()
    {
        $res = [];
        $rc  = new \ReflectionObject($this);
        foreach ($rc->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PUBLIC) as $rp) {
            if ('_constructed_' === $rp->getName() || '_options_' === $rp->getName()) {
                continue;
            }
            $rp->setAccessible(true);
            $res[$rp->getName()] = $rp->getValue($this);
            $rp->setAccessible(false);
        }

        return $res;
    }

    /**
     * Used to protect options from being modified after construction
     *
     * @internal
     */
    private bool $_constructed_ = false;

    /**
     * The options used to create this class initially, used by
     * {@see self::overrideFrom()}.
     *
     * @internal
     */
    private ?array $_options_ = null;
}
