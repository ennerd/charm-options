<?php
require 'vendor/autoload.php';

use Charm\AbstractOptions;

class Sub extends AbstractOptions {
    protected int $a;
    protected int $b;
}

class Top extends AbstractOptions {
    protected Sub $subOptions;
    protected int $c;
}

$top = Top::create([
    'subOptions' => [
        'a' => 1,
        'b' => 2,
    ],
    'c' => 3
]);

var_dump($top);
