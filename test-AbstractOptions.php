<?php
require 'vendor/autoload.php';

use Charm\AbstractOptions;

class TC extends AbstractOptions {

    protected $untyped;                             // not required, because PHP auto-initializes the value to `null`.
    protected int $opt_int;                         // required
    protected float $opt_float;                     // required
    protected bool $opt_bool;                       // required
    protected string $opt_string;                   // required
    protected ?string $opt_nullable_string;         // option is required, but can be set = null
    protected ?string $opt_optional_string = null;  // option is not required
    protected int $opt_enum = self::OPT_ENUM_AUTO;  // must match the OPT_ENUM_* constants

    /**
     * If there exists class constants with a prefix identical to a property name, we assume
     * the constants declare the only acceptable values for the property.
     */
    const OPT_ENUM_OFF = 0;
    const OPT_ENUM_AUTO = 1;
    const OPT_ENUM_ON = 2;
}

// succeeds
$c = new TC([
    "opt_int" => 123,
    "opt_float" => 123.45,
    "opt_bool" => true,
    "opt_string" => "123",
    "opt_nullable_string" => null,
]);

try {
    // fails because of TC::OPT_ENUM_* constants wrong
    $c = new TC([
        "opt_int" => 123,
        "opt_float" => 123.45,
        "opt_bool" => true,
        "opt_string" => "123",
        "opt_nullable_string" => null,
        "opt_enum" => 3,                // illegal value
    ]);
    throw \Exception("Failed because 'opt_enum' should have been captured");
} catch (\Charm\Options\IllegalValueException $e) {
}

$c = TC::create($c);

echo "All ok\n";
exit(0);
